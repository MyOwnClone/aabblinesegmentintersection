/*
All code is based on 
http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?print=1 or http://www.gamasutra.com/view/feature/3383/simple_intersection_tests_for_games.php
*/

#include "intersections.h"
#include <stdio.h>

int main()
{
	auto result = false;

	result = AABB_LineSegmentOverlap(/*dir*/ VECTOR(1, 1, 1), /* mid */VECTOR(0.5, 0.5, 0.5), /* half length*/ 5, AABB(/* position */VECTOR(0.1, 0.1, 0.1), /* extent */VECTOR(1, 1, 1)));
	printf("Result should be: 1 and it is: %d\n", result);

	result = AABB_LineSegmentOverlap(/*dir*/ VECTOR(1, 1, 1), /* mid */VECTOR(0.5, 0.5, 0.5), /* half length*/ 5, AABB(/* position */VECTOR(10, 10, 10), /* extent */ VECTOR(1, 1, 1)));	
	printf("Result should be: 0 and it is: %d\n", result);

	result = AABB_LineSegmentOverlap(/*dir*/ VECTOR(1, 1, 1), /* mid */VECTOR(0.5, 0.5, 0.5), /* half length*/ 15, AABB(/* position */VECTOR(10, 10, 10), /* extent */ VECTOR(1, 1, 1)));
	printf("Result should be: 1 and it is: %d\n", result);

	result = AABB_LineSegmentOverlap(/*dir*/ VECTOR(1, 1, 1), /* mid */VECTOR(0.5, 0.5, 0.5), /* half length*/ 9, AABB(/* position */VECTOR(10, 10, 10), /* extent */ VECTOR(1, 1, 1)));
	printf("Result should be: 1 and it is: %d\n", result);

	result = AABB_LineSegmentOverlap(/*dir*/ VECTOR(-1, -1, -1), /* mid */VECTOR(0.5, 0.5, 0.5), /* half length*/ 8, AABB(/* position */VECTOR(10, 10, 10), /* extent */ VECTOR(1, 1, 1)));
	printf("Result should be: 0 and it is: %d\n", result);

	return 0;
}