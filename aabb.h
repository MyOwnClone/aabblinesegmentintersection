/*
All code is based on
http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?print=1 or http://www.gamasutra.com/view/feature/3383/simple_intersection_tests_for_games.php
*/

#pragma once

#include "vector.h"
#include <math.h>

#define SCALAR double

// An axis-aligned bounding box

class AABB
{
public:
	VECTOR P; //position
	VECTOR E; //x,y,z extents
	AABB(const VECTOR& p, const VECTOR& e) : P(p), E(e)
	{}

	//returns true if this is overlapping b
	const bool overlaps(const AABB& b) const
	{
		const VECTOR T = b.P - P;//vector from A to B
		return fabs(T.x) <= (E.x + b.E.x)&& fabs(T.y) <= (E.y + b.E.y)	&& fabs(T.z) <= (E.z + b.E.z);
	}

	//NOTE: since the vector indexing operator is not const,
	//we must cast away the const of the this pointer in the
	//following min() and max() functions
	//min x, y, or z

	const SCALAR min(long i) const
	{
		return ((AABB*)this)->P[i] - ((AABB*)this)->E[i];
	}

	//max x, y, or z
	const SCALAR max(long i) const
	{
		return ((AABB*)this)->P[i] + ((AABB*)this)->E[i];
	}
};