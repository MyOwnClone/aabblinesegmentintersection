/*
All code is based on
http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?print=1 or http://www.gamasutra.com/view/feature/3383/simple_intersection_tests_for_games.php
*/

#pragma once
#include "vector.h"

class OBB 
{
public:
	VECTOR E; //extents
	OBB(const VECTOR& e) : E(e)
	{}
};
