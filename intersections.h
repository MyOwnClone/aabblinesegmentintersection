/*
All code is based on
http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?print=1 or http://www.gamasutra.com/view/feature/3383/simple_intersection_tests_for_games.php
*/

#pragma once

#include "aabb.h"

const bool AABBSweep
(
	const VECTOR&	Ea,	//extents of AABB A
	const VECTOR&	A0,	//its previous position
	const VECTOR&	A1,	//its current position
	const VECTOR&	Eb,	//extents of AABB B
	const VECTOR&	B0,	//its previous position
	const VECTOR&	B1,	//its current position
	SCALAR&	u0,	//normalized time of first collision
	SCALAR&	u1	//normalized time of second collision
	);

const bool OBBOverlap
(
	//A
	VECTOR&	a,	//extents
	VECTOR&	Pa,	//position
	VECTOR*	A,	//orthonormal basis
				//B
	VECTOR&	b,	//extents
	VECTOR&	Pb,	//position
	VECTOR*	B	//orthonormal basis
	);

const bool AABB_LineSegmentOverlap
(
	const VECTOR&	l,	//line direction
	const VECTOR&	mid,	//midpoint of the line segment
	const SCALAR	hl,	//segment half-length
	const AABB&	b	//box
	);