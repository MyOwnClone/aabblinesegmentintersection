/*
All code is based on
http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?print=1 or http://www.gamasutra.com/view/feature/3383/simple_intersection_tests_for_games.php
*/

#pragma once

class VECTOR
{
public:
	double x, y, z;

	VECTOR()
	{
		x = y = z = 0;
	}

	VECTOR(double x, double y, double z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
	
	double& operator[](int index)
	{
		switch (index)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			break;
		}
	}

	VECTOR operator+(const VECTOR other)
	{
		return VECTOR(x + other.x, y + other.y, z + other.z);
	}

	VECTOR operator-(const VECTOR& other) const
	{
		return VECTOR(x - other.x, y - other.y, z - other.z);
	}

	double dot(const VECTOR other)
	{
		return (x * other.x + y * other.y + z * other.z);
	}
};